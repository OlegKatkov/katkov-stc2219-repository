package HomeWork8.Util;
import HomeWork8.entity.Student;
import HomeWork8.entity.Teacher;
import org.hibernate.Session;
import org.hibernate.Transaction;
import java.util.List;

public class initDB {
    public static void init(Session session) {
        Transaction transaction = session.beginTransaction();
        Student student1 = Student.builder()
                .firstName("Oleg")
                .lastName("Igonin")
                .build();

        Student student2 = Student.builder()
                .firstName("Vasya")
                .lastName("Pupkin")
                .build();

        Student student3 = Student.builder()
                .firstName("Iezekel")
                .lastName("Vasgenovich")
                .build();


        session.save(student1);
        session.save(student2);
        session.save(student3);


        Teacher teacher1 = Teacher.builder()
                .firstName("Владимир")
                .lastName("Ленин")
                .subject("Ленинизм")
                .students(List.of(student1, student2))
                .build();

        Teacher teacher2 = Teacher.builder()
                .firstName("Иосиф")
                .lastName("Сталин")
                .subject("Сталинизм")
                .students(List.of(student2, student3))
                .build();

        Teacher teacher3 = Teacher.builder()
                .firstName("Карл")
                .lastName("Маркс")
                .subject("Марксизм")
                .students(List.of(student1, student3))
                .build();

        session.save(teacher1);
        session.save(teacher2);
        session.save(teacher3);

        transaction.commit();
    }
}

