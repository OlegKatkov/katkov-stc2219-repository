package HomeWork8;

import HomeWork8.Util.initDB;
import HomeWork8.entity.Student;
import HomeWork8.entity.Teacher;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class Main {
    public static void main(String[] args) {

        Configuration configuration = new Configuration();
        configuration.configure();

        try (Session session = configuration.buildSessionFactory().openSession()) {
            initDB.init(session);

            List<Teacher> teachers = session.createQuery("from Teacher").list();
            for (Teacher teacher : teachers) {
                System.out.print(teacher.getId() + " " + teacher.getFirstName() + " " + teacher.getLastName() + " преподаёт " + teacher.getSubject());
                System.out.println(" своим студентам:");
                for (Student student : teacher.getStudents()) {
                    System.out.println(student.getFirstName() + " " + student.getLastName());
                }
                System.out.println("*****************");
            }

            List<Student> students = session.createQuery("from Student").list();
            for (Student student : students) {
                session.refresh(student);
                System.out.print(student.getId() + " " + student.getFirstName() + " " + student.getLastName());
                System.out.println(" и его преподы:");
                for (Teacher teacher : student.getTeachers()) {
                    System.out.println(teacher.getFirstName() + " " + teacher.getLastName());
                }
                System.out.println("******************");

            }
        }
    }
}

