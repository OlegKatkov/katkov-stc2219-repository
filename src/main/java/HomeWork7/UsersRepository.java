package HomeWork7;

public interface UsersRepository {
    User findById(int id);
    void create(User user);
    void update(User user);
    void delete(int id);
    void print();
    void builder();
}

