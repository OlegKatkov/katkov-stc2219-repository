package HomeWork7;

public class Main {
    public static void main(String[] args) {

User user;

        UsersRepository usersRepository;
        usersRepository = new UsersRepositoryImpl();
        usersRepository.builder();
        usersRepository.print();

        user = usersRepository.findById(1);
        if (user != null) {
            System.out.println(user);
            System.out.println("===========================");
            user.setName("Владимир");
            user.setAge(27);
            usersRepository.update(user);
            usersRepository.print();
        }
        usersRepository.delete(2);
        usersRepository.print();
        user = new User("Петр", "Алексеевич", 30, true);
        usersRepository.create(user);
        usersRepository.print();
    }
}

