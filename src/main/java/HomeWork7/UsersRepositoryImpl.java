package HomeWork7;
import java.io.*;
import java.util.*;

public class UsersRepositoryImpl implements UsersRepository {
    private final HashMap<Integer, User> users = new HashMap<>();
    private int sequence_id = 0;
    private final String path;

    public UsersRepositoryImpl() {
        ClassLoader classLoader = Main.class.getClassLoader();
        path = classLoader.getResource("katkov-stc2219-repository/src/main/java/HomeWork7/User.txt").getPath();
        System.out.println(path);
    }

    public void builder() {
        String DELIMITER = "\\|";
        File fr = new File(path);try (Scanner scan = new Scanner(fr)) {
            while (scan.hasNextLine()) {
                String[] parts = scan.nextLine().split(DELIMITER);
                int index = Integer.parseInt(parts[0]);
                if (index > sequence_id) sequence_id = index;
                if (users.containsKey(index)) {
                    throw new RuntimeException("Index in input file is incorrect");
                } else {
                    users.put(index, new User(parts[1], parts[2], Integer.parseInt(parts[3]), Boolean.parseBoolean(parts[4])));
                }
            }
        } catch (FileNotFoundException fnfe) {
            System.out.println("File with users not found");
            throw new RuntimeException();
        }
    }

    @Override
    public User findById(int id) {
        return users.get(id);
    }

    @Override
    public void create(User user) {
        sequence_id += 1;
        users.put(sequence_id, user);
        saveFile();
    }

    @Override
    public void update(User user) {
        saveFile();
    }

    @Override
    public void delete(int id) {
        users.remove(id);
        saveFile();
    }

    public void print() {
        users.forEach((key, value) -> System.out.println(key + " " + value));
        System.out.println("===========================");
    }

    private void saveFile() {
        File file = new File(path);
        try (Writer writer = new FileWriter(file)) {
            users.forEach((key, value) -> writeToFile(writer, key + "|" + value.toString()));
        } catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
    }

    private void writeToFile(Writer fw, String str) {
        try {
            fw.write(String.format("%s%n", str));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

