package HomeWork7;

public class User {
    private String name;
    private String family;
    private int age;
    private boolean hasJob;

    public User(String name, String family, int age, boolean hasJob) {
        this.name = name;
        this.family = family;
        this.age = age;
        this.hasJob = hasJob;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setHasJob(boolean hasJob) {
        this.hasJob = hasJob;
    }

    @Override
    public String toString() {
        return name + "|" + family + "|" + age + "|" + hasJob;
    }

    public String getName() {
        return name;
    }

    public String getFamily() {
        return family;
    }

    public int getAge() {
        return age;
    }

    public boolean isHasJob() {
        return hasJob;
    }

}
