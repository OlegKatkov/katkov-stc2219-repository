package Attestation;
import java.io.*;
import java.util.*;

public class WordMap extends MainMap<String> implements MyMap {

    @Override
    public void loadData(String path) {
        File fr = new File(path);
        try (Scanner scan = new Scanner(fr)) {
            while (scan.hasNextLine()) {
                String[] parts = scan.nextLine().split("\\s+");
                for (String str : parts) {
                    String word = str.toLowerCase();
                    mainMap.put(word, mainMap.getOrDefault(word, 0) + 1);
                }
            }
        } catch (FileNotFoundException fnfe) {
            System.out.println("File not found");
            throw new RuntimeException();
        }
    }
}

