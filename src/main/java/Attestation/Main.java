package Attestation;
import java.io.File;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        final String path;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Укажите наименование файла и путь к нему");
        String userpath = scanner.nextLine();

        File file = new File(userpath);
        if (file.exists() && !file.isDirectory()) {
            path = userpath;
        } else {
            System.out.println("Файл не найден. Читаем файл по умолчанию");
            ClassLoader classLoader = Main.class.getClassLoader();
            path = classLoader.getResource("katkov-stc2219-repository/src/main/java/Attestation/resources/filetext.txt").getPath();
        }
        System.out.println("Для подсчета символов - введите 1, для подсчета слов - введите 2");
        String userinput = scanner.nextLine();
        if (userinput.equals("1")) {
            CharMap charMap = new CharMap();
            charMap.loadData(path);
            charMap.print();
            charMap.save2file("katkov-stc2219-repository/src/main/java/Attestation/resources/unsort_chars.txt");
            charMap.sort();
            charMap.print();
            charMap.save2file("katkov-stc2219-repository/src/main/java/Attestation/resources/sort_chars.txt");
        } else if (userinput.equals("2")) {
            WordMap wordMap = new WordMap();
            wordMap.loadData(path);
            wordMap.print();
            wordMap.save2file("unsort_words.txt");
            wordMap.sort();
            wordMap.print();
            wordMap.save2file("sort_words.txt");
        } else {
            System.out.println("Некорректный ввод");
        }
    }
}
