package Attestation;
import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public abstract class MainMap <T> {
    Map<T, Integer> mainMap = new HashMap<>();

    public void print() {
        mainMap.forEach((key, value) -> System.out.println(key + " - " + value));
        System.out.println("_________________________");
    }


    public void save2file(String path) {
        File file = new File(path);
        try (Writer writer = new FileWriter(file)) {
            mainMap.forEach((key, value) -> writeToFile(writer, key + " - " + value.toString()));

        } catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
    }

    void writeToFile(Writer fw, String str) {
        try {
            fw.write(String.format("%s%n", str));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public void sort(){
        mainMap = new TreeMap(mainMap);
    }
}

