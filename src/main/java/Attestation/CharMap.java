package Attestation;

import java.io.*;
public class CharMap extends MainMap<Character> implements MyMap {
    @Override
    public void loadData(String path) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(path));
            int c;
            while ((c = reader.read()) != -1) {
                char character = Character.toLowerCase((char) c);
                if (Character.isLetter(character) || Character.isDigit(character)) {
                    mainMap.put(character, mainMap.getOrDefault(character, 0) + 1);
                }
            }
            reader.close();
        } catch (
                IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

