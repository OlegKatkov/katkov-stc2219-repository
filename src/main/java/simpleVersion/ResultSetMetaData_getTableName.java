package simpleVersion;
import java.sql.Connection;
        import java.sql.DriverManager;
        import java.sql.ResultSet;
        import java.sql.ResultSetMetaData;
        import java.sql.SQLException;
        import java.sql.Statement;
public class ResultSetMetaData_getTableName {
    public static void main(String args[]) throws SQLException {
        //Registering the Driver
        Connection driverManagerConnection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/myBase",
                "postgres",
                "myPassword");
        //Getting the connection
        String mysqlUrl = "jdbc:postgresql://localhost/myBase";
        Connection con = DriverManager.getConnection(mysqlUrl, "postgres", "myPassword");
        System.out.println("Connection established......");
        //Creating the Statement
        Statement stmt = con.createStatement();
        //Query to retrieve records
        String query = "Select * from customers";
        //Executing the query
        ResultSet rs = stmt.executeQuery(query);
        //retrieving the ResultSetMetaData object
        ResultSetMetaData resultSetMetaData = rs.getMetaData();
        //Retrieving the column name
        String tableName = resultSetMetaData.getTableName(4);
        System.out.println("Name of the table : "+ tableName);


    }

}
