package simpleVersion;
import java.sql.*;
public class JDBCExample {

    public static void main(String args[]) {
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/myBase", "postgres", "myPassword");
            stmt = c.createStatement();

            String sql = "CREATE TABLE PHONEBOOK "
                    + "(id VARCHAR(20)  NOT NULL, "
                    + " first_name           VARCHAR(20)  NOT NULL, "
                    + " last_name            VARCHAR(20)  NOT NULL, "
                    + " PHONE                VARCHAR(20), "
                    + " EMAIL                VARCHAR(50))";
stmt.executeUpdate(sql);

            sql = "INSERT INTO PHONEBOOK "
                    + "VALUES (1, 'asf', 'asf', '123', 'sd@gmail.com')";
            stmt.executeUpdate(sql);
            sql = "INSERT INTO PHONEBOOK "
                    + "VALUES (1, 'asf', 'asf', '123', 'sd@gmail.com')";
            stmt.executeUpdate(sql);
            sql = "INSERT INTO PHONEBOOK "
                    + "VALUES (1, 'asf', 'asf', '1233', 'sd@gmail.com')";
            stmt.executeUpdate(sql);
            sql = "INSERT INTO PHONEBOOK "
                    + "VALUES (1, 'asf', 'asf', '124', 'sd@gmail.com')";;
            stmt.executeUpdate(sql);

            sql = "SELECT * FROM PHONEBOOK";
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                //Получаем значения
                int id = rs.getInt("id");
                String email = rs.getString("EMAIL");
                String phone = rs.getString("PHONE");
                String first = rs.getString("first_name");
                String last = rs.getString("last_name");

                //Вывод данных
                System.out.print("ID: " + id);
                System.out.print(", First name: " + first);
                System.out.print(", Last name: " + last);
                System.out.print(", Phone: " + phone);
                System.out.println(", Email: " + email);
            }
            rs.close();
            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
    }
}