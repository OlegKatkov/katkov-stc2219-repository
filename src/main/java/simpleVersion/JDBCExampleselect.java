package simpleVersion;
import java.sql.*;
public class JDBCExampleselect {

    public static void main(String args[]) {
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/myBase", "postgres", "myPassword");
            stmt = c.createStatement();

            String sql = "SELECT * FROM PHONEBOOK";
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                //Получаем значения
                int id = rs.getInt("id");
                String email = rs.getString("EMAIL");
                String phone = rs.getString("PHONE");
                String first = rs.getString("first_name");
                String last = rs.getString("last_name");

                //Вывод данных
                System.out.print("ID: " + id);
                System.out.print(", First name: " + first);
                System.out.print(", Last name: " + last);
                System.out.print(", Phone: " + phone);
                System.out.println(", Email: " + email);
            }
            rs.close();
            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
    }
}