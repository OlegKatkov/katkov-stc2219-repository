import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
public class RS_getTables {
    public static void main(String args[]) throws Exception {
        //Registering the Driver
        Connection driverManagerConnection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/myBase",
                "postgres",
                "myPassword");

        //Getting the connection
        String mysqlUrl = "jdbc:postgresql://localhost:5432/myBase";
        Connection con = DriverManager.getConnection(mysqlUrl, "postgres", "myPassword");
        System.out.println("Connection established......");
        //Retrieving the meta data object
        DatabaseMetaData metaData = con.getMetaData();
        String[] types = {"TABLE"};
        //Retrieving the columns in the database
        ResultSet tables = metaData.getTables(null, null, "%", types);
        while (tables.next()) {
            System.out.println(tables.getString("TABLE_NAME"));


        }
    }
}
