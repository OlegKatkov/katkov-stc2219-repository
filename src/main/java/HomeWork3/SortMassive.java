package HomeWork3;
import java.util.Arrays;
import java.util.Collections;
class SortMassive {
        public static void main(String[] args) {
            Integer[] hw = { 50, 1, 0, 23, 18, 10, -18, 0, 0, 5, 48 };
            System.out.print("Исходник:      ");
//            System.out.print(Arrays.toString(hw));
            for (int value : hw)    {
                System.out.print(value + ", ");
            }
            System.out.println();
            //Сортировка массива
            System.out.print("Отсортировано: ");
            Arrays.sort(hw, Collections.reverseOrder());
            for (int values : hw) {
                System.out.print(values + ", ");
            }
        }
    }
