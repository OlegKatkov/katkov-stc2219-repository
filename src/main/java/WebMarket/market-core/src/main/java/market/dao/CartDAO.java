import market.domain.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public abstract class CartDAO extends CrudRepository<Cart, Long>, JpaRepository<Cart, Long> {

}
